#include <TinyGPS++.h>
static const uint32_t GPSBaud = 9600;
TinyGPSPlus gps;
//#define ss Serial2 // Versi 3
#define ss Serial3 //Versi 4
String neoVal, gpsVal, dateVal, timeVal, valTimeDate, disVal, kecVal, sumVal;
double lg, lt;
int dateYear, dateMonth, dateDay;
int hourVal, minuteVal, secondVal;
float hlg, //longitude yang diacu
      hlt, //latitude yang diacu
      dis, //jarak (float)
      maxerror, //simpangan/error maksimum. digunakan ketika gps didiamkan untuk melihat lokasi loncat seberapa
      dlt, //delta latitude
      dlg; //delta longitude
int sum, //jumlah jarak
    kec; //kecepatan
int boot, //penahan data ketika booting, untuk data2 pertama tidak dianggap
    disint, //jarak (integer)
    kanan, //jumlah gerakan ke timur (berapa kali)
    kiri, //jumlah gerakan ke barat (berapa kali)
    atas, //jumlah gerakan ke utara (berapa kali)
    bawah, //jumlah gerakan ke selatan (berapa kali)
    awal, //waktu awal
    akhir, //waktu akhir. untuk menghitung detikan, agar pengecekan jarak dilakukan tiap 1 detik
    tkanan, //total gerakan ketimur
    tbawah, //total gerakan keselatan
    tkiri, //total gerakan kebarat
    tatas; //total gerakan keutara
//kalman presetup start (lg) - filter kalman untuk longitude
float varVolt = 0.25, ////responnya?
      varProcess = 0.4, //ini untuk memperhalus
      Pc = 0.0, G = 0.0, P = 1.0, Xp = 0.0, Zp = 0.0, Xe = 0.0;
//kalman presetup start (lt) - filter kalman untuk latitude
float varVolt2 = 0.25, ////responnya?
      varProcess2 = 0.4, //ini untuk memperhalus
      Pc2 = 0.0, G2 = 0.0, P2 = 1.0, Xp2 = 0.0, Zp2 = 0.0, Xe2 = 0.0;


#include <ArduinoJson.h>

const int pinSig[] = {A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A18, A19, A21, A22 };
#include <ADC.h>
ADC *adc = new ADC(); // adc object;
double sensAnalog[18];
int rpm;

unsigned long times;

void setup() {
  times = 0;
  InitSensor();
  InitGPS();
}

void loop() {
  RunGPS();
  if (millis() > times + 1000) {
    RunSensor();
  }
}

void RunRPM() {
  sensAnalog[13] = adc->analogRead(pinSig[13]);
  unsigned long   highTime,
           lowTime,
           timePeriod,
           freq,
           timeOut = 500000; //micros

  highTime = pulseIn(sensAnalog[13], HIGH, timeOut);
  lowTime = pulseIn(sensAnalog[13], LOW, timeOut);
  timePeriod = highTime + lowTime;
  timePeriod = timePeriod / 1000;
  freq = 1000 / timePeriod;
  rpm = freq / 2;
}

void InitGPS()
{
  boot = 0; // inisialisasi boot=0 artinya baru booting
  maxerror = 0; //max error awalnya nol
  awal = 0; akhir = 0; kec = 0; //waktu dan kecepatan juga nol
  tkiri = 0; //jumlah gerakan kebarat nol
  tkanan = 0; //jumlah gerakan ketimur nol
  tbawah = 0; //jumlah gerakan keselatan nol
  tatas = 0; //jumlah gerakan keutara nol

  ss.begin(GPSBaud);
  neoVal = "GPS Init";
  while (ss.available() > 0) {
    Serial.println("GPS Ashhiaappp");
    delay(500);
  }
}

void RunGPS()
{
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    neoVal = "No GPS detected: check wiring.";
    while (true);
  }
}

void displayInfo() {
  neoVal = String(F("Location: "));
  if (gps.location.isValid())
  {
    lt = gps.location.lat();
    lg = gps.location.lng();
  }
  else
  {
    gpsVal = "invloc";
  }

  neoVal += String(gpsVal);
  neoVal += String(F("Date/Time:"));

  if (gps.date.isValid())
  {
    dateYear = gps.date.year();
    dateMonth = gps.date.month();
    dateDay = gps.date.day();

    dateVal = String(dateYear);
    dateVal += "-";
    dateVal += String(dateMonth);
    dateVal += "-";
    dateVal += String(dateMonth);
    dateVal += "T";

    valTimeDate = String(dateVal);
    //    valTimeDate += "-";
  }
  else
  {
    dateVal = "invdate";
    valTimeDate = dateVal;
    valTimeDate += "-";
  }

  neoVal += String(dateVal);

  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) timeVal = "0";
    timeVal += String(gps.time.hour());
    timeVal += ":";

    if (gps.time.minute() < 10) timeVal += "0";
    timeVal += String(gps.time.minute());
    timeVal += ":";

    if (gps.time.second() < 10) timeVal += "0";
    timeVal += String(gps.time.second());

    if (gps.time.centisecond() < 10) timeVal += "0";
    //    timeVal += String(gps.time.centisecond());

    valTimeDate += timeVal;
  }
  else
  {
    timeVal = String(F("invtm"));
    valTimeDate += timeVal;
  }
  neoVal += String(timeVal);
}

float filterlg(float val) {
  Pc = P + varProcess;
  G = Pc / (Pc + varVolt);
  P = (1 - G) * Pc;
  Xp = Xe;
  Zp = Xp;
  Xe = G * (val - Zp) + Xp;
  return (Xe);
}
//kalman presetup end (lg)

float filterlt(float val2) {
  Pc2 = P2 + varProcess2;
  G2 = Pc2 / (Pc2 + varVolt2);
  P2 = (1 - G2) * Pc2;
  Xp2 = Xe2;
  Zp2 = Xp2;
  Xe2 = G2 * (val2 - Zp2) + Xp2;
  return (Xe2);
}
//kalman presetup end (lt)

void DistaceCal() {
  awal = secondVal; //variabel awal diisi dengan detikan dari GPS yang terbaca saat itu
  if (akhir == awal) //jika akhir==awal, maksudnya ketika detikan sudah berganti. akhir diisi dgn nilai awal+1, alias detik selanjutnya
  {
    //    Serial.println(awal);
    awal = secondVal;
    akhir = awal + 1; //ini akhir diisi dengan awal+1. kemudian awal akan berubah jadi == akhir kalo detikan dari GPS sudah berubah
    if (akhir > 59) akhir = 0; //kalau akhir melebihi 59 jadiin nol, karena 60 detik
    dis = sqrt((abs(hlg - lg) * abs(hlg - lg)) + (abs(hlt - lt) * abs(hlt - lt))) * 111319.5; //hitung distance dalam meter
    kec = dis; //kecepatan dalam m/s
    kec = kec * 2.4; //kec dalam km/h. sebenarnya dari m/s ke km/h dikalikan dengan 3.6 namun setelah dikalibrasi, didapatkan bahwa kecepatan sesungguhnya adalah 2/3 dari nilai yang terbaca, sehingga jd 2.4
  }
  dlg = lg - hlg; //selisih long non absolute, akhir-awal
  dlt = lt - hlt; //selisih lat non absolute, akhir-awal
  if (dlg != 0 || dlt != 0) //apabila ada perubahan longitude atau latitude
  {
    if (dlg < -0.00001) { //kalau gerak ke barat
      kiri = kiri + 1; kanan = 0; //barat tambah 1, gerakan ketimur gugur
    }
    if (dlg > 0.00001) { //kalau gerak ke timur
      kiri = 0; kanan = kanan + 1; //timur tambah 1, gerakan kebarat gugur
    }
    if (dlt < -0.00001) { //kalau gerak ke utara
      atas = atas + 1; bawah = 0; //utara tambah, selatan gugur
    }
    if (dlt > 0.00001) { //kalau gerak ke selatan
      atas = 0; bawah = bawah + 1; //selatan tambah, utara gugur
    }
  }
  if (dis > 2 && (kiri > 20 || kanan > 20 || atas > 20 || bawah > 20)) //gerak yang searah baru dianggap gerak , gerak yang dianggap adalah yang diatas 2 meter, karena sering loncat2 sekitar 1 meter
  {
    hlg = lg; //titik acuan long
    hlt = lt; //titik acuan lat
    //    Serial.print("jarak terakhir = ");
    disVal = String(dis, 6);
    //    Serial.print(dis, 6); //jarak, 6 angka belakang koma
    //    Serial.println(" meter");
    //    Serial.print("Kecepatan = ");
    kecVal = String(kec);
    //    Serial.print(kec); //kecepatan dalam km/h
    //    Serial.println("km/h");

    if (boot > 5) //data akan dianggap (dijumlah ke total) ketika sudah data ke 5
    {
      //if (maxerror<dis) maxerror=dis;
      //Serial.print("max error = ");
      //Serial.print(maxerror);
      //Serial.println(" meter");
      if (dis < 100000) sum = sum + dis; //kalo jarak yg kebaca ga sampe 100000 akan dianggep dan dijumlahkan
      //      Serial.print("jarak total = ");
      sumVal = String(sum, 6);
      //      Serial.print(sum, 6); //jarak total, 6 angka belakang koma
      //      Serial.println(" meter");

      tkiri = tkiri + kiri; //total gerakan kebarat
      tkanan = tkanan + kanan; //total gerakan ketimur
      tbawah = tbawah + bawah; //total gerakan keselatan
      tatas = tatas + atas; //total gerakan keutara
    }
    boot = boot + 1;
    //Serial.print("kiri "); Serial.println(tkiri);
    //Serial.print("kanan "); Serial.println(tkanan);
    //Serial.print("atas "); Serial.println(tatas);
    //Serial.print("bawah "); Serial.println(tbawah);
    kiri = 0; kanan = 0; atas = 0; bawah = 0;
  }
}

void InitSensor() {
  Serial.begin(9600);
  while (!Serial) continue;
  Serial.println("Matador Tracking is Running");
}

void RunSensor() {

  sensAnalog[0] = adc->analogRead(pinSig[0]);
  sensAnalog[1] = adc->analogRead(pinSig[1]);
  sensAnalog[2] = adc->analogRead(pinSig[2]);
  sensAnalog[3] = adc->analogRead(pinSig[3]);
  sensAnalog[4] = adc->analogRead(pinSig[4]);
  sensAnalog[5] = adc->analogRead(pinSig[5]);
  sensAnalog[6] = adc->analogRead(pinSig[6]);
  sensAnalog[7] = adc->analogRead(pinSig[7]);
  sensAnalog[8] = adc->analogRead(pinSig[8]);
  sensAnalog[9] = adc->analogRead(pinSig[9]);
  sensAnalog[10] = adc->analogRead(pinSig[10]);
  sensAnalog[11] = adc->analogRead(pinSig[11]);
  sensAnalog[12] = adc->analogRead(pinSig[12]);
  //13
  sensAnalog[14] = adc->analogRead(pinSig[14]);
  sensAnalog[15] = adc->analogRead(pinSig[15]);
  sensAnalog[16] = adc->analogRead(pinSig[16]);
  sensAnalog[17] = adc->analogRead(pinSig[17]);
  sensAnalog[18] = adc->analogRead(pinSig[18]);


  DynamicJsonBuffer  jsonBuffer(1000);
  JsonObject& root = jsonBuffer.createObject();
  root["device_key"] = "key1";
  root["latitude"] = String(lt);
  root["longitude"] = String(lg);
  root["velocity"] = String(kec);
  root["distance"] = String(sum); // dari dari Yuantok
  root["engine_speed"] = String(rpm); // PR RPM konfig sama si Imam Sudrajat
  root["engine_temperature"] = String(sensAnalog[1]); // PR dari pin analog sensor harus dikalibrasi
  root["fuel"] = String(sensAnalog[2]); //Pin Analog sensor
  root["power"] = String(sensAnalog[3]);
  root["operating"] = String(sensAnalog[4]);
  root["charging"] = String(sensAnalog[5]);
  root["oil_pressure"] = String(sensAnalog[6]);
  root["air_pressure"] = String(sensAnalog[7]);
  root["transmission_oil"] = String(sensAnalog[8]);
  root["oil_water_separation"] = String(sensAnalog[9]);
  root["pto_disengage"] = String(sensAnalog[10]);
  root["brake_fluid"] = String(sensAnalog[11]);
  root["air_filter"] = String(sensAnalog[12]);
  root["trailer_brake"] = String(sensAnalog[0]);
  root["auger"] = String(sensAnalog[14]);
  root["parking_light"] = String(sensAnalog[15]);
  root["left_steer"] = String(sensAnalog[16]);
  root["right_steer"] = String(sensAnalog[17]);
  root["preheating"] = String("0"); //PR
  root["low_beam"] = String("0"); //PR
  root["high_beam"] = String("0"); //PR
  root["differential_log"] = String("0"); //PR
  root["clearance_lamp"] = String("0"); //PR
  root["oil_replacement"] = String("0"); //PR
  root.prettyPrintTo(Serial);
  Serial.println("\n");
}
